## AWS AMI URL 

### https://console.aws.amazon.com/ec2/home?region=us-east-2#Images:visibility=public-images;ownerAlias=973714476881;sort=name

## YouTube Videos.

### https://www.youtube.com/playlist?list=PLLmNg_q93mex-Bn8eFDVb8Sn77Yj3e8OI

## Katakoda URL 

### https://katakoda.com/rkalluru

## Documentation

### https://documents-7031e.firebaseapp.com/

## Slack 

### https://raghudevopstrainings.slack.com


## Tool Installation Scripts.

### https://github.com/linuxautomations/labautomation/tree/master/tools

## Intros

### https://gitlab.com/cit-devops/intros